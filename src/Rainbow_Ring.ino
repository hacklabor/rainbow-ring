#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <time.h>

#define LED_PIN 5
#define LED_COUNT 24
#define BUTTON_PIN 4
#define MODE_NUM 7  // number of modes

#define MY_NTP_SERVER "de.pool.ntp.org"           
#define MY_TZ "CET-1CEST,M3.5.0/02,M10.5.0/03"  // https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
uint32_t violet = strip.Color(128, 0, 128);
uint32_t red = strip.Color(128, 0, 0);
uint32_t green = strip.Color(0, 128, 0);
uint32_t blue = strip.Color(0, 0, 128);
uint32_t black = strip.Color(0, 0, 0);

const char* ssid     = "wifi";
const char* password = "password";

time_t now;                         // this is the epoch
tm tm;                              // the structure tm holds time information in a more convient way

uint16_t n = 0;
uint16_t mode = 0;

//variables to keep track of the timing of recent interrupts
unsigned long button_time = 0;  
unsigned long last_button_time = 0; 

void ICACHE_RAM_ATTR ISR() {
  button_time = millis();
  if (button_time - last_button_time > 250) {
    Serial.println("button was pressed");
    mode = (mode + 1) % MODE_NUM;
    Serial.println(mode);
    last_button_time = button_time;
  }
}

void printTime() {
  time(&now);                       // read the current time
  localtime_r(&now, &tm);           // update the structure tm with the current time
  Serial.print("year:");
  Serial.print(tm.tm_year + 1900);  // years since 1900
  Serial.print("\tmonth:");
  Serial.print(tm.tm_mon + 1);      // January = 0 (!)
  Serial.print("\tday:");
  Serial.print(tm.tm_mday);         // day of month
  Serial.print("\thour:");
  Serial.print(tm.tm_hour);         // hours since midnight  0-23
  Serial.print("\tmin:");
  Serial.print(tm.tm_min);          // minutes after the hour  0-59
  Serial.print("\tsec:");
  Serial.print(tm.tm_sec);          // seconds after the minute  0-61*
  Serial.print("\twday");
  Serial.print(tm.tm_wday);         // days since Sunday 0-6
  if (tm.tm_isdst == 1)             // Daylight Saving Time flag
    Serial.print("\tDST");
  else
    Serial.print("\tstandard");
  Serial.println();
}

void showTime() {
  time(&now);                       // read the current time
  localtime_r(&now, &tm);           // update the structure tm with the current time
  strip.clear();

  
  int pos_sec = 2 * ((int)(tm.tm_sec / 5) + 1);
  strip.fill(red, pos_sec == 24 ? 0 : 1, pos_sec);

  int pos_min = 2 * (int)(tm.tm_min / 5) + 1;
  uint32_t color_min = strip.Color(0, 8 + 30 * (tm.tm_min % 5), 0);
  strip.fill(color_min, pos_min, 2);
  if (pos_min == 23) strip.setPixelColor(0, color_min);

  int pos_hour = 2 * (int)(tm.tm_hour % 12);
  strip.setPixelColor(pos_hour, blue);

  strip.show();
  delay(1000);   
}

void rainbow(int wait) {
  for (long firstPixelHue = 0; firstPixelHue < 5 * 65536; firstPixelHue += 256) {
    for (int i = 0; i < strip.numPixels(); i++) { // For each pixel in strip...
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show();
    delay(wait);   
  }
}

void colorRun(int wait, uint32_t color) {
  for (int i = 0; i < LED_COUNT; i++) {
    strip.clear();
    for (int j = 0; j < 4; j++) {
      strip.setPixelColor((i+j) % LED_COUNT, color);
    }
    strip.show();
    delay(wait); 
  }
  
}

void rainbowRun(int wait) {
  for (long hue = 0; hue < 5 * 65536; hue += 2048) {
    for (int i = 0; i < LED_COUNT; i++) {
      strip.clear();
      for (int j = 0; j < 4; j++) {
        strip.setPixelColor((i+j) % LED_COUNT, strip.gamma32(strip.ColorHSV(hue, 255, 150)));
      }
      strip.show();
      delay(wait); 
    }
  }
  
}

void colorWipe(int wait, uint32_t color) {
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(wait);
  }
}

void theaterChase(int wait, uint32_t color) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

void theaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (strip.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show();                // Update strip with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  }
}

void setup() {
  Serial.begin(115200);
  configTime(MY_TZ, MY_NTP_SERVER);

  // initialize the pushbutton pin as an pull-up input
  // the pull-up input pin will be HIGH when the switch is open and LOW when the switch is closed.
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  attachInterrupt(BUTTON_PIN, ISR, FALLING);

  // Setup LED strip
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)

  // Connect to Wi-Fi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    colorWipe(20, violet);
    colorWipe(20, black);
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  
}


void loop() {

  switch (mode) {
    case 0:
      strip.rainbow(n,1,255,150,true);
      strip.show();
      n+=10;
      break;
 
    case 1:
      theaterChase(100, blue);
      theaterChase(100, red);
      theaterChase(100, green);
      break;
      
    case 2:
      theaterChaseRainbow(100);
      break;
      
    case 3:
      colorRun(100, blue);
      colorRun(100, red);
      colorRun(100, green);
      break;
          
    case 4:
      colorWipe(100, blue);
      colorWipe(100, red);
      colorWipe(100, green);  
      break;
        
    case 5:
      showTime();
      break;
    
    case 6:
      strip.clear();
      strip.show();
      printTime();
      break;
  }

  /*for (int i = 0; i < LED_COUNT; i++) {
      strip.setPixelColor(i, strip.Color(255,   0,   0));
  }
  strip.show();
  */

}
