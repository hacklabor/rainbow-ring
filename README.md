# Rainbow-Ring

**Projekt zur Eröffnung des Jugend hackt Lab Schwerin**

![](rainbow-ring.jpg)

| Wemos D1 mini | Ws2812b LED Pixel | Ring Button |
| ------------- | ----------------- | ----------- |
| D1            | DI                | -           |
| GND           | GND               | PIN 1       |
| 5V            | 5V                | -           |
| D2            | -                 | PIN 2       |
